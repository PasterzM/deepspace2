START TRANSACTION;

DROP DATABASE Deepspace_swiat0;
CREATE DATABASE Deepspace_swiat0;


CREATE TABLE Gracze(
	gracz_id INT NOT NULL AUTO_INCREMENT,
	login VARCHAR(100) UNIQUE,
	haslo VARCHAR(100),
	email VARCHAR(100) UNIQUE,
	kolonie_ids JSON,
	wiadomosci_ids JSON,
	raporty_ids JSON,
	komunikaty_id JSON,
	punkty INT,
	gildia_id INT,
	premium_punkty INT,
	premium_koniec DATETIME,
	ostatnia_aktualizacja DATETIME,	#w celu aktualizacji bazy raz na jakiś czas
PRIMARY KEY (gracz_id)
);

#INSERT INTO 'gracze'('gracz_id', 'login', 'haslo', 'email', 'premium_punkty', 'kolonie_ids') 
#VALUES (1, 'a', 'a', 'a@test.com', 9999, '[1,2]');
#INSERT INTO 'gracze'('gracz_id', 'login', 'haslo', 'email', 'premium_punkty', 'kolonie_ids') 
#VALUES (2, 'b', 'b', 'b@test.com', 9999, '[2]');


CREATE TABLE Kolonia(
	kolonia_id INT NOT NULL AUTO_INCREMENT,
	poz_X INT UNIQUE,
	poz_Y INT UNIQUE,
	budynki JSON,      
		#lista: {kod_budynku: lvl, ... }
		commandCenter	#centrum_dowodzenia_kolonia				
		brothel			#burdel (morale)
		commandArmy		#dowodzenie_armia
		officersQuarters#kwatery_oficerskie (rekrutacja bohatera)
		material1Extraction#wydobycie_surowca1, 2, 3
		supply			#zaopatrzenie (wyzywienie)
		storage			#magazyn (storage surowców)
		fence			#ogrodzenie (mur obronny)
		defensiveTurrets#wiezyczki_obronne
		commandHeadquarters#siedziba_dowodcy
		armory			#zbrojownia
		market			#rynek
		barracks		#koszary
		factory			#fabryka
		advancedFactory	#zaawansowana_fabryka
	jednostki JSON, #lista: kod_jednostki: sztuk
		#4 typy piechoty (np. marine, jednostka specjalna)
		#4 typy jednostek zaawansowanych(zwiadowca, mech, myśliwiec, niszczyciel)
		#2 typy jednostek burzących (ciężka artyleria, czołg)
		#generał (bohater popr. statystyki jednostek z którymi przebywa)
		#dowódca (jednostka zdolna do przejmowania kolonii)
		#cechy każdej jednostki: atak, obrona ogólna, obrona przeciw jedn. z fabryki(zaawansowane), obrona ranged, prędkość ruchu(min na przebycie 1 pola), pojemnosc_łupu, bazowy_koszt_rekrutacji, zużycie_zaopatrzenia
	surowiec1 INT,
	surowiec2 INT,
	surowiec3 INT,
	surowiec4 INT,
	pojemnosc_magazynu INT,
	id_gracza INT,		#RENDUNDANTNE ale potrzebne
	ostatnia_aktualizacja DATETIME,
	PRIMARY KEY (kolonia_id)
);
/* 
INSERT INTO kolonia( 'poz_X', 'poz_Y', 'surowiec1', 'surowiec2', 'surowiec3', 'pojemnosc_magazynu', 'id_gracza', 'budynki') 
VALUES (0, 0, 9999, 9999, 9999, 9999, 1,
	'{
	"centrum_dowodzenia_kolonia":1,
	"burdel":1,
	"centrum_dowodzenia_armia":1,
	"kwatery_oficerskie":1,
	"wydobycie_surowca1":1,
	"wydobycie_surowca2":1,
	"wydobycie_surowca3":1,
	"zaopatrzenie":1,
	"magazyn":1,
	"ogrodzenie":1,
	"wiezyczki_obronne":1.
	"siedziba_dowodcy":1,
	"zbrojownia":1,
	"rynek":1,
	"koszary":1,
	"fabryka":1,
	"ciezka_fabryka":1
	}'	
);
*/
 
CREATE TABLE Wiadomosci(
	wiadomosc_id INT NOT NULL AUTO_INCREMENT,
	temat VARCHAR(255),
	nadawca_id INT,
	odbiorca_id INT,		 #REDUNDANTNE ale w celu weryfikacji 
	tresc TEXT,			 #max 64KB per wiadomosc
	PRIMARY KEY (wiadomosc_id)
);


CREATE TABLE Raporty(
	raport_id INT NOT NULL AUTO_INCREMENT,
	temat VARCHAR(255),
	tresc TEXT, 
	PRIMARY KEY (raport_id)
);


CREATE TABLE Komunikaty(
	komunikat_id INT NOT NULL AUTO_INCREMENT,
	tresc TEXT, 
	PRIMARY KEY (komunikat_id)
);


CREATE TABLE Atak(
	atak_id INT NOT NULL AUTO_INCREMENT,
	atakujacy_id INT,
	atakowany_id INT,
	czas_wyslania DATETIME,
	czas_dotarcia DATETIME,
	jednostki JSON,
	PRIMARY KEY (atak_id)
);
COMMIT; #END TRANSACTION

