<?php
	session_start();
?>

<!doctype html>
<html lang="pl">
	<head>
		<meta charset="UTF-8">
		<title>...</title>
		<link rel="stylesheet" href="css/gra.css" />
		<link rel="stylesheet" href="css/fontello.css" />
	</head>
	<body>
	
	
	<div id="container">
		<div id="top_bar">
			<?php include 'navigation.php';	?><br><br>
		</div>			
			
		<div id="view">
			<?php
				$view = isset($_GET['view'])?$_GET['view']:'colony'; //get paramter 
				
				switch ($view){
					//main view:
					case "colony":
						include 'views/colony.php';	break;
					//buildings:
					case "commandCenter":
						include 'views/commandCenter.php'; break;
					case "brothel":
						include 'views/brothel.php'; break;
					case "commandArmy":
						include 'views/commandArmy.php'; break;
					case "officersQuarters":
						include 'views/officersQuarters.php'; break;
					case "material1Extraction":
						include 'views/material1Extraction.php'; break;
					case "material2Extraction":
						include 'views/material2Extraction.php'; break;
					case "material3Extraction":
						include 'views/material3Extraction.php'; break;
					case "material4Extraction":
						include 'views/material4Extraction.php'; break;
					case "supply":
						include 'views/supply.php'; break;
					case "storage":
						include 'views/storage.php'; break;
					case "fence":
						include 'views/fence.php'; break;
					case "defensiveTurrets":
						include 'views/defensiveTurrets.php'; break;
					case "commanderHeadquarters":
						include 'views/commanderHeadquarters.php'; break;
					case "armory":
						include 'views/armory.php'; break;
					case "market":
						include 'views/market.php'; break;
					case "barracks":
						include 'views/barracks.php'; break;
					case "factory":
						include 'views/factory.php'; break;
					case "advacedFactory":
						include 'views/advacedFactory.php'; break;
					//pages:
					case "reports":
						include 'views/reports.php'; break;
					case "messages": 
						include 'views/messages.php'; break;
					case "guild":
						include 'views/guild.php'; break;
					case "map":
						include 'views/map.php'; break;
					case "settings":
						include 'views/settings.php'; break;
				
				}
			?>
		</div>

		<?php //right_bar
			if($view=='colony'){
				echo '<div id="right_bar">';
				include 'right_bar.php';	
				echo '</div>';
			}
		?>
		
		<div id="bottom_bar">
			<?php include 'footer.php';	?>
		</div>
		
	</body>
</html>