<?php
	session_start();
	
	if ((!isset($_POST['login'])) || (!isset($_POST['haslo']))){
		header('Location: index.php');
		exit();
	}

	
	//LACZCENIE SIE Z BAZA DANYCH
	require_once "connect.php";
	$polaczenie = @new mysqli($host, $db_user, $db_password, $db_name);
	if ($polaczenie->connect_errno!=0){	
		echo "Error: ".$polaczenie->connect_errno;
		exit();
	}else{	
		try{
			$login = $_POST['login'];
			$haslo = $_POST['haslo'];		
			$login = htmlentities($login, ENT_QUOTES, "UTF-8");
			$haslo = htmlentities($haslo, ENT_QUOTES, "UTF-8");
					
			if ($rezultat = @$polaczenie->query(
			sprintf("SELECT * FROM gracze WHERE login='%s' AND haslo='%s'",
			mysqli_real_escape_string($polaczenie,$login),
			mysqli_real_escape_string($polaczenie,$haslo)))){
				if($rezultat->num_rows>0){
					$gracz=$rezultat->fetch_assoc();
					$_SESSION['gracz_id'] = $gracz['gracz_id'];
					$_SESSION['kolonie_ids'] = $gracz['kolonie_ids'];
					$_SESSION['wiadomosci_ids'] = $gracz['wiadomosci_ids'];
					$_SESSION['raporty_ids'] = $gracz['raporty_ids'];
					$_SESSION['komunikaty_id'] = $gracz['komunikaty_id'];
					$_SESSION['punkty'] = $gracz['punkty'];
					$_SESSION['gildia_id'] = $gracz['gildia_id'];
					$_SESSION['premium_punkty'] = $gracz['premium_punkty'];
					$_SESSION['premium_koniec'] = $gracz['premium_koniec'];
					

	
			//pobranie koloni gracza
					$kolonia_sql = "SELECT * FROM kolonia WHERE id_gracza=".$gracz['gracz_id'];
					if($rezultat = @$polaczenie->query($kolonia_sql)){					
						if($rezultat->num_rows > 0){
							//TODO: dodoanie wiekszej ilosci kolonij
							$kolonia = $rezultat->fetch_assoc();					
							$_SESSION['surowiec1'] = $kolonia['surowiec1'];
							$_SESSION['surowiec2'] = $kolonia['surowiec2'];
							$_SESSION['surowiec3'] = $kolonia['surowiec3'];
							$_SESSION['surowiec4'] = $kolonia['surowiec4'];
							$budynki = json_decode($kolonia['budynki']);
							//dodanie budynkow, indeksem jest nazwa budynku a wartoscia poziom
							$size = sizeof($budynki);
							//echo $size;
							for ($i = 0; $i < $size ; $i+=2) {
								$_SESSION['budynki'][$budynki[$i]] = $budynki[$i+1];
							}
							//echo "<br/>";
							//foreach ($_SESSION['budynki'] as $key => $value) {
							//	echo "{$key} => {$value} "."<br/>";
							//}
							$_SESSION['surowiec3'] = $kolonia['surowiec3'];					
							//$_SESSION['surowiec4'] = $kolonia['surowiec4'];		
						}
					}					
					$rezultat->close();
					header('Location:gra.php');
				}else{
					if(!$gracz_rezultat) throw new Exception($polaczenie->error);
					echo "Nie ma w bazie takiego bracza <br/>";
				}
			}else{
				echo "bląd składni <br/>";
				if(!$rezultat){throw new Exception($polaczenie->error);}
			}		
			$polaczenie->close();
			$_SESSION['zalogowany'] = true;
		}catch(Exception $ex){
			echo $ex;
		}
	}
?>
