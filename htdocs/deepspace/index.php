<?php

	session_start();
	//jezeli uzytkownik jest zalogowany, przerzuć go do gry
	if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true))
	{
		header('Location: gra.php');
		exit();
	}

?>

<!doctype html>
<html lang="pl">
	<head>
		<meta charset="UTF-8">
		<title>...</title>
		<link rel="stylesheet" href="css/index.css" />
	</head>
	<body>
	<div id="panel">
		<div id="lewyPanel">
			<form action="zaloguj.php" method="post">
				Login: <br /> <input type="text" name="login" value="test"/> <br />
				Hasło: <br /> <input type="password" name="haslo" value="test"/> <br /><br />
				<input type="submit" value="Zaloguj" />
			</form>
		</div>
		<div id="prawyPanel">
			<a href="zarejestruj.php">Zarejestruj się</a><br/>
				<?php if(isset($_SESSION['CzyUdanaRejestracja'])){
					echo '<div class="accepted"> '.$_SESSION['CzyUdanaRejestracja'].'</div>';
					unset($_SESSION['CzyUdanaRejestracja']);
				}?>
		</div>
	</div>
	</body>
</html>