<?php
	session_start();
	//jezeli uzytkownik jest zalogowany, przerzuć go do gry
	if ((isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany']==true)){
		header('Location: gra.php');
		exit();
	}
	//rejestracja
	require_once "connect.php";
	mysqli_report(MYSQLI_REPORT_STRICT);		
	
	//wcisniecie przycisku rejestruj
	if(isset($_POST['email']) && isset($_POST['login']) && isset($_POST['haslo1']) && isset($_POST['haslo2'])){
		
		$wszystko_OK = true;
		$login = $_POST["login"];
		$email = $_POST["email"];
		$haslo1 = $_POST["haslo1"];
		$haslo2 = $_POST["haslo2"];
		
		//LACZCENIE SIE Z BAZA DANYCH
		$polaczenie = @new mysqli($host, $db_user, $db_password, $db_name);
		if ($polaczenie->connect_errno!=0){	
			//echo "Error: ".$polaczenie->connect_errno;
			exit();
		}
		
		try{
			//SPRAWDZANIE POPRAWNOSCI DANYCH
			//sprawdzenie znakow 
			if(ctype_alnum($login)==false){
				$wszystko_OK = false;
				$_SESSION['e_login']="login może składać się tylkoe z liter i cyfr";
			}
			//sprawdzenei dlugosci loginu
			if(strlen($login)<3 || strlen($login)>20){
				$wszystko_OK = false;
				$_SESSION['e_login']="login musi posiadac od 3 do 20 znakow";
			}
			//sprzwdzenie czy w bazie jest juz uzytkownik z podanym loginem
			if($login_z_bazy = $polaczenie->query("SELECT * FROM gracze WHERE login='$login'")){	//wybranie gracza				
					//echo "liczba użytkowników z podanym loginem w bazie: $login_z_bazy->num_rows <br/>";
					if($login_z_bazy->num_rows > 0){$wszystko_OK = false; $login_z_bazy->free();}		
			}else{
				throw new Exception($polaczenie->error);
			}
			
			//sprawdzenie poprawnosci znakow w mailu
			$emailValidation = filter_var($email,FILTER_SANITIZE_EMAIL);
			if(filter_var($email,FILTER_SANITIZE_EMAIL)==false || $emailValidation!=$email){
				$wszystko_OK = false;
				$_SESSION['e_email']="Podaj poprawny edres email";
			}
			
			//sprawdzenie czy hasla sa rowne
			if($haslo1 != $haslo2){
				$wszystko_OK = false;
				$_SESSION['e_haslo']="Podane hasła są różne";
			}
			
			if( strlen($haslo1)==0){
				$wszystko_OK = false;
				$_SESSION['e_haslo']="Nie podano hasła";
			}
				
			//TWORZENIE GRACZA
			if($wszystko_OK==true){	
				//stworzenie gracza
				if(!$polaczenie->query("INSERT INTO `gracze`(`login`, `haslo`, `email`) VALUES ('$login','$haslo1','$email')")){
					throw new Exception($polaczenie->error);
				}
				//wybranie gracza
				$gracz_rezultat = $polaczenie->query("SELECT * FROM gracze WHERE login='$login' AND haslo='$haslo1'");			
				if(!$gracz_rezultat) throw new Exception($polaczenie->error);
				
				$gracz = $gracz_rezultat->fetch_assoc();
				echo $gracz_rezultat->num_rows."<br/>";
				
				//kolonia
				$id = $gracz['gracz_id'];
				if(!$polaczenie->query("INSERT INTO  kolonia (`id_gracza`, `surowiec1`, `surowiec2`, `surowiec3`, `budynki`) VALUES ($id,300,300,300,JSON_ARRAY('centrumDowodzenia',1,'magazyn',1,'zaopatrzenie',1))")){	//stworzenie gracza
					throw new Exception($polaczenie->error);
				}
				$kolonia_rezultat = $polaczenie->query("SELECT * FROM kolonia WHERE id_gracza=".$gracz['gracz_id']);
				if(!$kolonia_rezultat) throw new Exception($polaczenie->error);
				$kolonia = $kolonia_rezultat->fetch_assoc();
				echo $kolonia_rezultat->num_rows;
				//update gracza
				$kolonia_id = $kolonia['kolonia_id'];
				if(!$polaczenie->query("UPDATE gracze SET kolonie_ids = ".$kolonia['kolonia_id']." WHERE gracz_id=$id")){	//update gracza
					throw new Exception($polaczenie->error);
				}				
				$gracz_rezultat->free();
				$kolonia_rezultat->free();
				$polaczenie->close();
				$_SESSION['CzyUdanaRejestracja'] = "Udalo się zarejestrować konto, dodać uwierzytelnianie!!";
				header('Location: index.php');
			}else{
				;
				//echo "Coś poszło nie tak przy testach wartości.<br/>";
				/*
				echo '<div class="error">'.$_SESSION['e_login'].'</div>';
				echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
				echo '<div class="error">'.$_SESSION['e_email'].'</div>';
				*/
			}
		}catch(Exception $ex){
				echo $ex;
		}	
	}
?>

<!DOCTYPE HTML>
<html lang="pl">
	<head>
		<meta charset="UTF-8">
		<title>...</title>
		<link rel="stylesheet" href="css/index.css" />
	</head>
	<body>
	<div id="panel">
		<div id="lewyPanel">
			<form method="post">
				Login: <br /> <input type="text" name="login" /> <br />
				<?php if(isset($_SESSION['e_login'])){
					echo '<div class="error">'.$_SESSION['e_login'].'</div>';
					unset($_SESSION['e_login']);
				} ?>				
				email: <br /> <input type="text" name="email" /> <br />
					<?php if(isset($_SESSION['e_email'])){
					echo '<div class="error">'.$_SESSION['e_email'].'</div>';
					unset($_SESSION['e_email']);
				} ?><br /><br/>
				Hasło: <br /> <input type="password" name="haslo1" /> <br /><br />
					<?php if(isset($_SESSION['e_haslo'])){
					echo '<div class="error">'.$_SESSION['e_haslo'].'</div>';
					unset($_SESSION['e_haslo']);
				} ?>
				potwierdz haslo: <br /> <input type="password" name="haslo2" /> <br /><br />				
				<input type="submit" id="utworzKonto" value="DodajKonto" />
			</form>
		</div>
	</div>
	</body>
</html>